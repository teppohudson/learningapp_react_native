import React, { Component, Fragment } from 'react';
import {View, Text, Keyboard} from 'react-native';
import { Input, TextLink, Loading, Button } from './common';
import deviceStorage from '../services/deviceStorage';
import ApiKeys from '../constants/ApiKeys'
import * as firebase from 'firebase';
import { ScrollView } from 'react-native-gesture-handler';

const root = ApiKeys.FiboAPIConfig.root;

// const CryptoJS = require("crypto-js");

export default class Registration extends Component {
  constructor(props){
    super(props);
    this.state = {
      email: '',
      password: '',
      password_confirmation: '',
      error: '',
      loading: false
    };

    this.registerUser = this.registerUser.bind(this);
    this.onRegistrationFail = this.onRegistrationFail.bind(this);
    this.onkeyboardpress = this.onkeyboardpress.bind(this);
  }

  registerUser() {
    const { email, password, password_confirmation } = this.state;
    // const passwordcipher = CryptoJS.SHA256(password).toString();
    // const passwordcipher_confirmation = CryptoJS.SHA256(password_confirmation).toString();

    console.log('email',email);
    console.log('password',password);
    console.log('password_confirmation',password_confirmation);
    this.setState({ error: ''});

    // const ax = {
    //   email: email,
    //   password: passwordcipher,
    //   password2:passwordcipher_confirmation
    // }

    if (password === password_confirmation) {
      firebase.auth().createUserWithEmailAndPassword(email, password)
          .then((responseJson)=>{
            console.log('user created2',responseJson)
            this.props.navigateHome(responseJson.user)

          })
          .catch((error) => {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            if (errorCode === 'auth/weak-password') {
              this.setState({error:errorMessage})
              // alert('The password is too weak.');
            } else {
              this.setState({error:errorMessage})
              // alert(errorMessage);
            }
            console.log(error);
          });
    } else {
      /** should notify the user about passwords */
    }


    /** NOTE this is FiboAPI integration */
    // fetch(root+"/learningapp/signup",{
    //   method: 'POST',
    //   headers: {
    //     Accept: 'application/json',
    //     'Content-Type': 'application/json',
    //   },
    //   body: JSON.stringify(ax)
    // }).then((response) => {
    //   return response.json()  //we only get here if there is no error
    // }).then((responseJson) => {
    //   console.log('responseJson',responseJson);
    //   if (responseJson.status === 200){
    //     deviceStorage.saveKey("id_token", responseJson.token);
    //     this.props.newJWT(responseJson.token);
    //   } else {
    //     this.onRegistrationFail(responseJson.message)
    //   }
    //
    // }).catch((error) => {
    //   console.log(error);
    //   this.onRegistrationFail();
    // });
  }

  onRegistrationFail = (msg) => {
    let err_message = (msg) ? msg : 'Registration Failed'
    this.setState({
      error: err_message,
      loading: false
    });
  }

  onkeyboardpress () {
    Keyboard.dismiss()
    setTimeout(()=>{
      this.registerUser()
    }, 500)
  }

  render() {
    const { email, password, password_confirmation, error, loading } = this.state;
    const { form, section, errorTextStyle, switchButton } = styles;

    return (
        <Fragment>
              <View style={form}>
                <View style={section}>
                  <Input
                      placeholder="user@email.com"
                      label="Email"
                      value={email}
                      onSubmitEditing = {this.onkeyboardpress}
                      onChangeText={email => this.setState({ email })}
                  />
                </View>

                <View style={section}>
                  <Input
                      secureTextEntry={true}
                      placeholder="password"
                      label="Password"
                      value={password}
                      onSubmitEditing = {this.onkeyboardpress}
                      onChangeText={password => this.setState({ password })}
                  />
                </View>

                <View style={section}>
                  <Input
                      secureTextEntry={true}
                      placeholder="confirm password"
                      label="Confirm Password"
                      value={password_confirmation}
                      onSubmitEditing = {this.onkeyboardpress}
                      onChangeText={password_confirmation => this.setState({ password_confirmation })}
                  />
                </View>

                <Text style={errorTextStyle}>
                  {error}
                </Text>

                {!loading ?
                    <Button onPress={this.registerUser}>
                      Register
                    </Button>
                    :
                    <Loading size={'large'} />
                }
              </View>
              <TextLink onPress={this.props.authSwitch} style={switchButton}>
                Already have an account? Log in!
              </TextLink>
        </Fragment>

    );
  }
}

const styles = {
  form: {
    width: '100%',
    borderTopWidth: 1,
    borderColor: '#ddd',
  },
  section: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    backgroundColor: '#fff',
    borderColor: '#ddd',
  },
  errorTextStyle: {
    alignSelf: 'center',
    fontSize: 18,
    color: 'red',
    padding: 20
  },
    switchButton: {
        paddingTop: 20,
    }
};
