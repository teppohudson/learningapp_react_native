import React, { Component, Fragment } from 'react';
import {View, Text, Keyboard} from 'react-native';
import { Input, TextLink, Loading, Button } from './common';
import deviceStorage from '../services/deviceStorage';
import ApiKeys from '../constants/ApiKeys'
import * as firebase from 'firebase';

const root = ApiKeys.FiboAPIConfig.root;

// const CryptoJS = require("crypto-js");

export default class Login extends Component {
    constructor(props){
        super(props);
        this.state = {
            email: '',
            password: '',
            error: '',
            loading: false
        };

        // this.loginUser = this.loginUser.bind(this);
        // this.onLoginFail = this.onLoginFail.bind(this);
    }

    loginUser = () => {
        const { email, password } = this.state;

        const passwordcipher = null
        // const passwordcipher = CryptoJS.SHA256(password).toString();

        this.setState({ error: '', loading: true });

        const ax = {
            username: email,
            password: passwordcipher
        }

        firebase.auth().signInWithEmailAndPassword(email, password)
            .then((response)=>{
                let that = this;
                console.log('signin created2',response)
                Keyboard.dismiss()
                this.props.navigateHome(response.user)

            })
            .catch((error) => {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                // ...
                this.onLoginFail(errorMessage)
            });

        /** NOTE this is FiboAPI integration */
        // console.log(root+"/learningapp/login")
        // fetch(root+"/learningapp/login", {
        //     method: 'POST',
        //     headers: {
        //         Accept: 'application/json',
        //         'Content-Type': 'application/json',
        //     },
        //     body: JSON.stringify(ax)
        // }).then((response) => {
        //     return response.json()  //we only get here if there is no error
        // }).then((responseJson) => {
        //     console.log('responseJson',responseJson);
        //     if (responseJson.status === 200){
        //         deviceStorage.saveKey("id_token", responseJson.token);
        //         this.props.newJWT(responseJson.token);
        //     } else {
        //         this.onLoginFail(responseJson.message);
        //     }
        // }).catch((error) => {
        //     console.log(error);
        //     this.onLoginFail();
        // });
    }

    onLoginFail = (msg) => {
        let err_message = (msg) ? msg : 'Login Failed'
        this.setState({
            error: err_message,
            loading: false
        });
    }

    onkeyboardpress = () => {
        Keyboard.dismiss()
        setTimeout(()=>{
            this.loginUser()
        }, 500)

    }

    render() {
        const { email, password, error, loading } = this.state;
        const { form, section, errorTextStyle, switchButton } = styles;

        return (
            <Fragment>
                <View style={form}>
                    <View style={section}>
                        <Input
                            placeholder="username or email"
                            label="Email"
                            value={email}
                            onSubmitEditing = {this.onkeyboardpress}
                            onChangeText={email => this.setState({ email })}
                        />
                    </View>

                    <View style={section}>
                        <Input
                            secureTextEntry
                            placeholder="password"
                            label="Password"
                            value={password}
                            onSubmitEditing = {this.onkeyboardpress}
                            onChangeText={password => this.setState({ password })}
                        />
                    </View>

                    <Text style={errorTextStyle}>
                        {error}
                    </Text>

                    {!loading ?
                        <Button onPress={this.loginUser}>
                            Login
                        </Button>
                        :
                        <Loading size={'large'} />
                    }

                </View>
                <TextLink onPress={this.props.authSwitch} style={switchButton}>
                    Don't have an account? Register!
                </TextLink>
            </Fragment>
        );
    }
}

const styles = {
    form: {
        width: '100%',
        borderTopWidth: 1,
        borderColor: '#ddd'
    },
    section: {
        flexDirection: 'row',
        borderBottomWidth: 1,
        backgroundColor: '#fff',
        borderColor: '#ddd',
    },
    errorTextStyle: {
        alignSelf: 'center',
        fontSize: 18,
        color: 'red',
        padding: 20,
    },
    switchButton: {
        paddingTop: 20,
    }
};
