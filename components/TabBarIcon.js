import * as React from 'react';
import { Ionicons } from '@expo/vector-icons';
import { Badge, Icon, withBadge } from 'react-native-elements';

import Colors from '../constants/Colors';

export default function TabBarIcon(props) {
  if (props.badgeSize && props.badgeSize > 0){
    const MessagesBadge = withBadge(props.badgeSize)(Icon)
    return (
        <MessagesBadge
            type="ionicon"
            name={props.name}
            size={30}
            style={{ marginBottom: -3 }}
            color={props.focused ? Colors.tabIconSelected : Colors.tabIconDefault}
        />

    )

  } else {
    return (
        <Ionicons
            name={props.name}
            size={30}
            style={{ marginBottom: -3 }}
            color={props.focused ? Colors.tabIconSelected : Colors.tabIconDefault}
        />

    );
  }
}

