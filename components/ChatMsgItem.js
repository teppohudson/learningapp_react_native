import * as React from 'react';
import {View, Text, Keyboard, StyleSheet} from 'react-native';


export default function ChatMsgItem({text, auth, byme}) {
    if (byme){
        return(
            <View style={styles.msgFirst}>
                <View style={styles.chatRow}>
                    <View style={styles.me}>
                        <Text style={styles.meText}>{text}</Text>
                    </View>
                </View>
            </View>
        )
    } else {
        return (
            <View style={styles.msgFirst}>
                <View style={styles.chatRow}>
                    <View style={styles.you}>
                        <Text style={styles.youText}>{text}</Text>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    msgFirst:{
        marginTop:10,
    },
    chatRow:{
        width:'100%',
        paddingTop:2,
        paddingRight:10,
        paddingBottom:2,
        paddingLeft:10,
    },
    you: {
        width:'75%',
        borderRadius:7,
        // backgroundColor: 'red',
    },
    youText: {
        borderRadius:7,
        overflow:"hidden",
        flexWrap: 'wrap',
        // backgroundColor: '#e18aa7',
        backgroundColor: '#ffffff',
        color:'#404041',
        alignSelf: 'flex-start',
        paddingTop:5,
        paddingRight:10,
        paddingBottom:5,
        paddingLeft:10,
        fontSize: 16
    },
    me: {
        width:'75%',
        alignSelf: 'flex-end',
        borderRadius:7,
        // backgroundColor: 'red',
        shadowColor: '#afafaf',
        shadowOffset: {
            width: 0,
            height: 1
        },
        shadowRadius: 2,
        shadowOpacity: 0.6
    },
    meText: {
        borderRadius:7,
        overflow:"hidden",
        textAlign: 'left',
        alignSelf: 'flex-end',
        flexWrap: 'wrap',
        // backgroundColor: '#dcf8c6',
        backgroundColor: '#f8d8c6',
        paddingTop:5,
        paddingRight:10,
        paddingBottom:5,
        paddingLeft:10,
        fontSize: 16
    },
})
