import React from 'react';
import {createStackNavigator} from "@react-navigation/stack";
const RootStack = createStackNavigator();
import * as firebase from "firebase";

import BottomTabNavigator from './BottomTabNavigator';
import AuthScreen from "../screens/Auth";
import SettingsScreen from "../screens/SettingsScreen";
import ModalNewSessionScreen from "../screens/ModalNewSessionScreen";

export const AppNavigator = (initialRoute) => {

    return (
        <RootStack.Navigator {...initialRoute} headerMode='none'>
            <RootStack.Screen name="Auth" component={AuthScreen} />
            <RootStack.Screen name="Home" component={BottomTabNavigator} />
            <RootStack.Screen name="Settings" component={SettingsScreen} />
            <RootStack.Screen name="NewSession" component={ModalNewSessionScreen} />
        </RootStack.Navigator>
    )
};

