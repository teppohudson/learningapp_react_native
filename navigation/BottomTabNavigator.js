import * as React from 'react';

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';

import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/HomeScreen';
import SettingsScreen from '../screens/SettingsScreen';
import ChatOpportunityScreen from '../screens/ChatOpportunityScreen';
import ChatListScreen from '../screens/ChatListScreen';
import ChatScreen from '../screens/ChatScreen';
import * as firebase from "firebase";

const BottomTab = createBottomTabNavigator();
const INITIAL_ROUTE_NAME = 'Home';
const Stack = createStackNavigator();

function ChatStack () {
    return(
        <Stack.Navigator
            initialRouteName="Chatlist"
            headerMode="screen"
        >
            <Stack.Screen
                name="Chatlist"
                component={ChatListScreen}
                options={{
                    title: 'Chats',
                    headerShown:false
                }}
            />
            <Stack.Screen
                name="Chat"
                component={ChatScreen}
            />
        </Stack.Navigator>
    )
}

export default function BottomTabNavigator({ navigation, route }) {
  // Set the header title on the parent stack navigator depending on the
  // currently active tab. Learn more in the documentation:
  // https://reactnavigation.org/docs/en/screen-options-resolution.html
  navigation.setOptions({ headerTitle: getHeaderTitle(route) });


    const [isTutor, setIsTutor] = React.useState(false);
    const [jobs, setJobs] = React.useState(0);
    const [chats, setChats] = React.useState(0);

    React.useEffect(() => {
        let user = firebase.auth().currentUser
        firebase.database().ref('/new_tutor_requests/').on('value', function(snapshot) {
            let newdata = []
            let snapshots = snapshot.val() || null
            if (snapshots !== null){
                // firebase.database().ref('/current_tutor_active_waiting/' + user.uid).once('value', function(snapshot){
                //     let active_snapshots = snapshot.val()
                //     if (active_snapshots){
                //         Object.keys(active_snapshots).forEach(function(key){
                //             if (snapshots[key]){
                //                 if (new Date().getTime() - snapshots[key].createdAt < (60*60*1000)) { /** check that is not more than 2h since */
                //                     snapshots[key].active = true;
                //                 }
                //             }
                //         })
                //     }
                //     if (snapshots){
                //         Object.keys(snapshots).forEach((key)=>{
                //             let itemdata = snapshots[key];
                //             itemdata._id = key;
                //             if (itemdata._id !== firebase.auth().currentUser.uid && !itemdata.active) { /** don't push user own item */
                //                 if (new Date().getTime() - itemdata.createdAt < (60*60*1000)) { /** check that is not more than 2h since */
                //                     newdata.push(itemdata)
                //                 }
                //             }
                //         });
                //     }
                //     setJobs(newdata.length);
                // });

                Object.keys(snapshots).forEach((key)=>{
                    let itemdata = snapshots[key];
                    itemdata._id = key;
                    if (itemdata._id !== firebase.auth().currentUser.uid && !itemdata.active) { /** don't push user own item */
                        if (new Date().getTime() - itemdata.createdAt < (2*60*60*1000)) { /** check that is not more than 2h since */
                        newdata.push(itemdata)
                        }
                    }
                });
                setJobs(newdata.length);
            } else {
                setJobs(newdata.length);
            }
        });

        firebase.database().ref('/chats_unread/' + user.uid).on('value', function(snapshot) {
            setChats(snapshot.val())
        });

        firebase.database().ref('/users/' + user.uid).once('value', function(snapshot){
            let userinfo = snapshot.val();
            if (userinfo){
                setIsTutor(userinfo.isTutor)
            }
        });


        firebase.database().ref('/new_sessions/'+user.uid).on('value', function(snapshot){
            /** need timestamp to know which was the latest update and if happened in the last 10 sec*/

            console.log(snapshot.val())

            if (snapshot.val()){
                navigation.navigate('NewSession')
            }
        });


        return function cleanup() {
            firebase.database().ref('/new_tutor_requests/').off();
            firebase.database().ref('/new_sessions/'+user.uid).off();
            // firebase.database().ref('/chats_unread/' + user.uid).off();
        };
    }, []);

    const _checkIfTutor = () => {
        if (isTutor){
            return (
                <BottomTab.Screen
                    name="Jobs"
                    component={ChatOpportunityScreen}
                    options={{
                        title: 'Jobs',
                        tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} name="md-list" badgeSize={jobs} />,
                    }}
                />
            )
        } else {
            return null
        }
    }

  return (
    <BottomTab.Navigator initialRouteName={INITIAL_ROUTE_NAME}>
      <BottomTab.Screen
        name="Home"
        component={HomeScreen}
        options={{
          title: 'Get a tutor',
          tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} name="md-school" />,
        }}
      />
        {_checkIfTutor()}
        <BottomTab.Screen
            name="Chats"
            component={ChatStack}
            options={{
                title: 'Chats',
                tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} name="md-chatboxes" badgeSize={chats} />,
            }}
        />
      <BottomTab.Screen
        name="Settings"
        component={SettingsScreen}
        options={{
          title: 'Settings',
          tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} name="md-settings" />,
        }}
      />
    </BottomTab.Navigator>
  );
}

function getHeaderTitle(route) {
  const routeName = route.state?.routes[route.state.index]?.name ?? INITIAL_ROUTE_NAME;

  switch (routeName) {

      case 'Home':
          return 'Learning App';
      case 'Jobs':
          return 'Jobs';
      case 'Chats':
          return 'Chats';
    case 'Settings':
        return 'Settings';

  }
}
