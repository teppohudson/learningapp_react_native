import React, { Component } from 'react';
import { View, Image } from 'react-native';
import Login from '../components/Login';
import Registration from '../components/Registration';

export default function AuthScreen({navigation, reset, showSignup_reset}) {

  const [showSignup, setShowSignup] = React.useState(showSignup_reset);
  const [JWT, setJWT] = React.useState(null);

  const authSwitch = () => {
    setShowSignup(!showSignup)
  };

  const newJWT = (jwt) => {
    // PERFECT SPOT TO IMPLEMENT REDUX IF NEEDED
    /** currently we have kinda redux style "devidestorage" created -> managed in Login and Registration components */

    console.log('newJWT in app')
    setJWT(jwt);

    const location = jwt ? 'Home' : 'Auth';
    navigation.navigate(location, {'jwt':JWT});

  };

  const navigateHome = (user) => {
    navigation.navigate('Home');
  }

  /** needs also missing password logic */
  const whichForm = () => {
    if(!showSignup){
      return(
          <Login navigateHome={navigateHome} newJWT={newJWT} authSwitch={authSwitch} reset={reset} />
      );
    } else {
      return(
          <Registration navigateHome={navigateHome} newJWT={newJWT} authSwitch={authSwitch} />
      );
    }
  }

  return (
      <View style={styles.container}>
        <Image
            style={{ width: "100%", height: "40%", position: 'absolute', top: 70, left: 0 }}
            source={require('../assets/images/splash_icon.png')}
        />
        {whichForm()}
      </View>
  )
}


// export class Auth extends Component {
//   constructor(props){
//     super(props);
//     this.state = {
//         showSignup: false
//     };
//     this.whichForm = this.whichForm.bind(this);
//     this.authSwitch = this.authSwitch.bind(this);
//   }
//
//   authSwitch() {
//     this.setState({
//       showSignup: !this.state.showSignup
//     });
//   }
//
//   newJWT = (jwt) => {
//     // PERFECT SPOT TO IMPLEMENT REDUX IF NEEDED
//     /** I have kinda redux style "devidestorage" created */
//
//     console.log('newJWT in app')
//     this.setState({
//       jwt: jwt
//     });
//
//     const location = jwt ? 'Home' : 'Auth';
//     this.props.navigation.navigate(location, {'jwt':jwt});
//
//   }
//
//   whichForm() {
//     if(!this.state.showSignup){
//       return(
//           <Login newJWT={this.newJWT} authSwitch={this.authSwitch} />
//       );
//     } else {
//       return(
//           <Registration newJWT={this.newJWT} authSwitch={this.authSwitch} />
//       );
//     }
//   }
//
//   render() {
//     return(
//       <View style={styles.container}>
//           <Image
//               style={{ width: "100%", height: "40%", position: 'absolute', top: 70, left: 0 }}
//               source={require('../assets/images/splash_icon.png')}
//           />
//         {this.whichForm()}
//       </View>
//     );
//   }
// }

const styles = {
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: "#F8E8E0"
  }

};
