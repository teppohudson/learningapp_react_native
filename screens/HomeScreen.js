import * as React from 'react';
import { Image, Platform, SafeAreaView, StyleSheet, Text, TouchableOpacity, View, TextInput, Dimensions, ActivityIndicator, Keyboard } from 'react-native';
import {Card, Button, ListItem, Rating, AirbnbRating} from 'react-native-elements';

// import { Input, TextLink, Loading, Button } from '../components/common';
import { ScrollView } from 'react-native-gesture-handler';
import * as WebBrowser from 'expo-web-browser';
import { MonoText } from '../components/StyledText';
import RNPickerSelect from "react-native-picker-select";
import { EvilIcons } from '@expo/vector-icons';
import Timer from '../services/timer';
import deviceStorage from '../services/deviceStorage';
import * as firebase from 'firebase';
import dismissKeyboard from "react-native-web/dist/modules/dismissKeyboard";
import Subject from '../constants/Subject_name'
import Level from '../constants/School_level'

export default function HomeScreen({navigation}) {
  const [levels, setLevels] = React.useState([{label:'7. Grade', value: 'yla_7', key:0},{label:'8. Grade', value: 'yla_8', key:0},{label:'9. Grade', value: 'yla_9', key:0}, {label:'High School 1', value: 'lukio_1', key:0}, {label:'High School 2', value: 'lukio_2', key:0}, {label:'High School 3', value: 'lukio_3', key:0}]);
  const [level, setLevel] = React.useState('');
  const [subjects, setSubjects] = React.useState([{label:'Mathematics', value: 'math', key:0},{label:'Physics', value: 'physics', key:0},{label:'Chemistry', value: 'chemistry', key:0}]);
  const [subject, setSubject] = React.useState('');
  const [comment, setComment] = React.useState('');

  const [userPrefs, setUserPrefs] = React.useState('');
  const [showSearch, setShowSearch] = React.useState(false);
  const [isSearching, setIsSearching] = React.useState(true);
  const [currentTutorList, setCurrentTutorList] = React.useState({});

  React.useEffect(() => {
    /** one time initial hook */

    /** here fetch user preferences */
    let userId = firebase.auth().currentUser.uid;
    firebase.database().ref('/users/' + userId).once('value').then(function(snapshot) {
      let usersnap = snapshot.val()
      if (usersnap !== null){
        setUserPrefs(snapshot.val())
      }
    });

    return function cleanup() {
    };

  }, []);

  // React.useEffect(() => {
  //   setLevel(userPrefs.level)
  //   setSubject(userPrefs.subject)
  // }, [userPrefs]);

  React.useEffect(() => {
    // if (!userPrefs.school_level || !userPrefs.school_subject) return;
    let updates = {}
    let user = firebase.auth().currentUser

    if (userPrefs.school_level) updates['/users/'+user.uid+ '/school_level'] = userPrefs.school_level
    if (userPrefs.school_subject) updates['/users/'+user.uid+ '/school_subject'] = userPrefs.school_subject
    firebase.database().ref().update(updates);

    // firebase.database().ref('/users/' + userId).set({...userPrefs,
    //   school_level: userPrefs.school_level,
    //   school_subject: userPrefs.school_subject,
    // })
  }, [userPrefs]);

  const _writeNewUserRequest = (config) => {
    let userId = firebase.auth().currentUser.uid;
    firebase.database().ref('/users/'+userId).once('value',(snapshot)=>{
      let userinfo = snapshot.val()
      if (!userinfo) userinfo = {}
      userinfo.name = userinfo.name || null
      userinfo.img = userinfo.img || null
      firebase.database().ref('/new_tutor_requests/' + userId).set({
        createdAt: new Date().getTime(),
        name: userinfo.name,
        img:userinfo.img,
        school_level: config.school_level,
        school_subject : config.school_subject,
        comment: config.comment
      })
    })
  };

  const _removeUserRequest = () => {
    let userId = firebase.auth().currentUser.uid;
    firebase.database().ref('/new_tutor_requests/' + userId).remove()
  };

  const _listenTutorResponses = () => {
    /** listen for tutor responses */
    let userId = firebase.auth().currentUser.uid;
    firebase.database().ref('/tutor_responses_lists/' + userId).on('value', (snapshot) => {
      console.log('snapshot homescreen:',snapshot)
      let responses_data = []
      let responses = snapshot.val()
      if (responses){
        // let responses = snapshot
        responses_data = Object.keys(responses).map((key) => {
          console.log('key',key)
          let item = responses[key];
          item._id = key;
          return item
        })
        console.log(responses_data)
        setCurrentTutorList(responses_data);
      }
    });
  };
  const _removeTutorResponsesListening = () => {
    let userId = firebase.auth().currentUser.uid;
    firebase.database().ref('/tutor_responses_lists/'  + userId).off();
  }

  const _acceptTutor = (_id) => {
    console.log('accept id:',_id)
    let userId = firebase.auth().currentUser.uid;
    firebase.database().ref('/new_tutor_requests/'+userId).once('value').then(function(snapshot){
      let user_request = snapshot.val()
      let request_info = user_request || {}

      // let chat_session_path_student = '/chats_student/'  + userId + '/' + _id + '/sessions/';
      // let chat_session_path_tutor = '/chats_tutor/'  + _id + '/' + userId + '/sessions/';

      let chats = '/chats/';
      var newChatKey = firebase.database().ref().child(chats).push().key;

      var updates = {};
      let update_time = new Date().getTime()
      let members = {}
      members[_id] = true
      members[userId] = true

      updates[chats + newChatKey] = {
        createdAt: update_time,
        name: null,
        last_sessions: null,
        comment: request_info.comment
      };
      updates['/members/'+newChatKey] = members;
      // updates['/messages/'+newChatKey] = ;
      updates['/user_chats/'+ userId + '/' + newChatKey] = true;

      /**todo: this is a data insecurity ->> need server side function to create chats on other profiles or some other acceptance method */
      updates['/user_chats/'+ _id + '/' + newChatKey] = true;
      updates['/user_matched/'+ userId + '/' + _id] = true;

      let value = 0
      firebase.database().ref('/chats_unread/'+userId).once('value', function(snapshot){
        let server_value = snapshot.val();
        value = server_value ? server_value + 1 : 1;
        updates['/chats_unread/'+userId] = value;

        firebase.database().ref('/chats_unread/'+_id).once('value', function(snapshot){
          let server_value = snapshot.val();
          value = server_value ? server_value + 1 : 1;
          updates['/chats_unread/'+_id] = value;

          // updates[chat_session_path_student + newSessionKey] = true
          // updates[chat_session_path_tutor + newSessionKey] = true

          firebase.database().ref().update(updates).then(() => {

            /** todo remove from:
             * current_tutor_waiting
             * new_tutor_requests
             * tutor_responses_lists
             */
            firebase.database().ref('/new_tutor_requests/'+userId).remove()
            // firebase.database().ref('/current_tutor_active_waiting/'+_id+'/'+userId).remove()
            firebase.database().ref('/tutor_responses_lists/'+userId+'/'+_id).remove()

            setCurrentTutorList(currentTutorList.filter(item => item._id !== _id))

            _onBigButtonPress();

            /** update finished -> navigate to chat */

          });
        });
      });
    })
  };

  const placeholder_levels = {
    label: 'Select a school level...',
    value: null,
    color: '#9EA0A4',
  };
  const placeholder_subjects = {
    label: 'Select a subject...',
    value: null,
    color: '#9EA0A4',
  };

  const _renderSchool_subject = (item) => {
    if (!item.school_subject) return null
    return (<Text style={[styles.strong, {fontSize:16, paddingTop:5, paddingBottom: 5}]}>{Subject[item.school_subject].en}</Text>)
  }

  const _renderSchool_level = (item) => {
    if (!item.school_level) return null
    return (<Text style={[{paddingBottom: 15}]}>{Level[item.school_level].en}</Text>)
  }

  const ratingText = () => {
    return <Text>
      <Text style={{fontSize:16, fontWeight:'700'}}>4.7 </Text>
      <Rating
          imageSize={20}
          readonly
          startingValue={2}
          ratingCount={1}
          type={'custom'}
          // ratingColor={'#4B4B4B'}
      />
    </Text>

  }

  const renderNewTutorList = () => {
    let tutorsElements = []
    if (currentTutorList){
      Object.keys(currentTutorList).forEach(function(key, index){
        if (currentTutorList[key]){
          let item = currentTutorList[key];
          if (new Date().getTime() - item.createdAt > (60*60*1000)) return /** check that is not more than 2h since */
          let username = item.name || "Anonymous";
          let image = {
            title:item.name[0]
          }
          if (item.img){image.source = {uri:item.img}}

          let tutorName = item.name || 'anonymous';
          tutorsElements.push(
              <View style={styles.card} key={item._id}>
                <View>
                  <ListItem
                      leftAvatar = {image}
                      title = {username}
                      subtitle={ratingText()}
                      subtitleStyle={{fontStyle:'italic'}}
                  />
                  <Text>{'"'+item.about+'"'}</Text>
                  {_renderSchool_subject(item)}
                  {_renderSchool_level(item)}
                </View>

                <View style={styles.button_container}>
                  <TouchableOpacity key={item._id} onPress={() => _acceptTutor(item._id)} style={{flex:1}}>
                    <View style={styles.button_green}><Text style={[styles.dark_text, styles.strong]}>OK</Text></View>
                  </TouchableOpacity>

                </View>

              </View>
          )
        }
      });
    }
    return tutorsElements
  }

  const renderActivity = () => {
    if (isSearching){
      return(
          <View>
            <Text style={{alignSelf: 'center'}}><ActivityIndicator size="small" color="#207C98" /></Text>
            <Text style={{alignSelf: 'center'}}><Text>Searching...</Text></Text>
          </View>
      )
    } else {
      return null
    }
  };

  const renderSearch = () => {
    if (showSearch){
      return (
          <View style={{flex:1}}>
            <ScrollView>
              <View style={{paddingTop:20,paddingLeft:20,paddingRight:20}}>
                <Text style={styles.strong}>{Subject[userPrefs.school_subject].en}</Text>
                <Text>{Level[userPrefs.school_level].en}</Text>
                <Text>{comment}</Text>
              </View>
                {renderNewTutorList()}
            </ScrollView>
            <View style={styles.absoluteBottom}>
              {renderActivity()}
              <TouchableOpacity onPress={_onBigButtonPress}>
                <View style={styles.btn_full}>
                  <Text style={styles.btn_text}>
                    Cancel
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
      )
    } else {
      return (
          <View style={[{flex:1}]}>
            {/*<View style={styles.card}>*/}
              <Card>
              <View style={{paddingVertical:20}}>
                <Text>Select school level</Text>
                {/* and iOS onUpArrow/onDownArrow toggle example */}
                <RNPickerSelect
                    placeholder={placeholder_levels}
                    items={levels}
                    onValueChange={value => {
                      setUserPrefs({...userPrefs,school_level:value})
                    }}
                    style={pickerSelectStyles}
                    value={userPrefs.school_level}
                    Icon={() => {
                      return <EvilIcons name="chevron-down" size={32} color="gray" style={{marginTop:10}} />;
                    }}
                />
              </View>
              <View style={{paddingVertical:20}}>
                <Text>Select school subject</Text>
                {/* and iOS onUpArrow/onDownArrow toggle example */}
                <RNPickerSelect
                    placeholder={placeholder_subjects}
                    items={subjects}
                    onValueChange={value => {
                      setUserPrefs({...userPrefs,school_subject:value})
                    }}
                    style={pickerSelectStyles}
                    value={userPrefs.school_subject}
                    Icon={() => {
                      return <EvilIcons name="chevron-down" size={32} color="gray" style={{marginTop:10}} />;
                    }}
                />
              </View>
              <ScrollView style={{paddingVertical:20}}>
                <Text>Add a comment to the tutor</Text>
                <TextInput
                    style={styles.textInputIOS}
                    autoCompleteType='off'
                    multiline={true}
                    placeholder='comment...'
                    placeholderTextColor = 'lightgray'
                    numberOfLines={4}
                    onChangeText={(text) => setComment(text)}
                    onSubmitEditing={()=>Keyboard.dismiss()}
                    returnKeyType='done'
                    value={comment}/>
              </ScrollView>
              </Card>
            {/*</View>*/}

            <View style={styles.bigButton_wrap}>
              <TouchableOpacity onPress={_onBigButtonPress}>
                <View style={styles.bigButton}>
                </View>
              </TouchableOpacity>
            </View>
          </View>
      )
    }
  };

  const _onBigButtonPress = () => {
    if (showSearch){

      setShowSearch(false)
      setCurrentTutorList({})
      setIsSearching(true)
      /** remove search actions */
      _removeTutorResponsesListening()
      _removeUserRequest()

    } else {
      setShowSearch(true)
      /** init new search */
      let cleanComment = comment.replace(/(\r\n|\n|\r)/gm, "");
      let config = {
        school_level: userPrefs.school_level,
        school_subject: userPrefs.school_subject,
        comment: cleanComment,
      }

      console.log('config:',config)
      _writeNewUserRequest(config);
      _listenTutorResponses()

    }
  };

  /** main render */
  return (
      <SafeAreaView style={styles.container}>

        {renderSearch()}

      </SafeAreaView>
  );
}

HomeScreen.navigationOptions = {
  header: null
};

function DevelopmentModeNotice() {
  if (__DEV__) {
    // const learnMoreButton = (
    //   <Text onPress={handleLearnMorePress} style={styles.helpLinkText}>
    //     Learn more
    //   </Text>
    // );
    //
    // return (
    //   <Text style={styles.developmentModeText}>
    //     Development mode is enabled: your app will be slower but you can use useful development
    //     tools. {learnMoreButton}
    //   </Text>
    // );
  } else {
    // return (
    //   <Text style={styles.developmentModeText}>
    //     You are not in development mode: your app will run at full speed.
    //   </Text>
    // );
  }
}

function handleLearnMorePress() {
  WebBrowser.openBrowserAsync('https://docs.expo.io/versions/latest/workflow/development-mode/');
}

function handleHelpPress() {
  WebBrowser.openBrowserAsync(
      'https://docs.expo.io/versions/latest/get-started/create-a-new-app/#making-your-first-change'
  );
}

var width = Dimensions.get('window').width;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    // backgroundColor: '#FFF6F0'
  },
  topSpace:{
    marginTop: 50
  },
  bigButton_wrap: {
    position: 'absolute',
    bottom: 30,
    alignSelf: 'center'
  },
  text: {
    color: 'black'
  },
  text_white: {
    color: 'white'
  },
  backgroundImage: {
    width:'100%',
    height:'100%',
    position: 'absolute'
  },
  absoluteBottom: {
    position: 'absolute',
    bottom: 0,
    alignSelf: 'center'
  },
  btn : {
    padding: 20,
    backgroundColor: '#6bacef',
    width: width*0.5,
    color: 'white'
  },
  btn_full : {
    padding: 20,
    backgroundColor: '#6bacef',
    width: width,
    color: 'white'
  },
  btn_text: {
    alignSelf: 'center',
    color: 'white'
  },
  bigButton: {
    backgroundColor: '#6bacef',
    width: width*0.5,
    height: width*0.5,
    borderRadius: width*0.5,
    alignSelf:'center',
    marginBottom:20,
    shadowColor: "#333",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 0.55,
    shadowRadius: 5.84,

    elevation: 5,
  },
  button_container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  button_green: {
    backgroundColor: '#6EE8B3',
    height: 40,
    flex: 1, justifyContent: 'center',
    alignItems: 'center'
  },
  button_red: {
    backgroundColor: '#E78564',
    height: 40,
    marginRight: 10,
    flex: 1, justifyContent: 'center',
    alignItems: 'center'
  },
  dark_text : {
    color: '#4B4B4B'
  },
  white_text : {
    color: '#FFFFFF'
  },
  strong: {
    fontWeight: "700"
  },
  textInputIOS: {
    fontSize: 16,
    minHeight: 100,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  card: {
    margin: 15,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 4,
    backgroundColor: '#ffffff',
    shadowColor: "#333",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  }
});

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: 16,
    paddingVertical: 12,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: 4,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: 16,
    paddingHorizontal: 10,
    paddingVertical: 8,
    borderWidth: 0.5,
    borderColor: 'purple',
    borderRadius: 8,
    color: 'black',
    paddingRight: 30, // to ensure the text is never behind the icon
  }
});