import React from 'react';

import {
    View,
    Text,
    TouchableOpacity,
    SafeAreaView,
    Modal,
    TouchableHighlight,
    SectionList,
    ActivityIndicator,
    TextInput,
    StyleSheet,
    Keyboard
} from 'react-native';
import { Card, Input, Button } from 'react-native-elements';

import deviceStorage from "../services/deviceStorage";
import * as firebase from 'firebase';
import RNPickerSelect from "react-native-picker-select";
import {ScrollView} from "react-native-gesture-handler";

export default function SettingsScreen({navigation}) {
    const [levels, setLevels] = React.useState([{label:'7. Luokka', value: 'yla_7', key:0},{label:'8. Luokka', value: 'yla_8', key:0},{label:'9. Luokka', value: 'yla_9', key:0}, {label:'Lukio 1', value: 'lukio_1', key:0}, {label:'Lukio 2', value: 'lukio_2', key:0}, {label:'Lukio 3', value: 'lukio_3', key:0}]);
    const [level, setLevel] = React.useState('');
    const [subjects, setSubjects] = React.useState([{label:'Mathematics', value: 'math', key:0},{label:'Physics', value: 'physics', key:0},{label:'Chemistry', value: 'chemistry', key:0}]);
    const [subject, setSubject] = React.useState('');

    const [name, setName] = React.useState('')
    const [showSaved, setShowSaved] = React.useState(false)
    const [tutorApplied, setTutorApplied] = React.useState(false)

    React.useEffect(() => {
        if (firebase.auth().currentUser){
            let userId = firebase.auth().currentUser.uid
            setName(firebase.auth().currentUser.displayName)

            firebase.database().ref('/tutor_applications/'+userId).once('value', function(snapshot){
                let userapplied = snapshot.val()
                if (userapplied){
                    setTutorApplied(true)
                }
            });
        }
        return function cleanup() {
        };
    }, []);


    const _logout = () => {
        firebase.auth().signOut().then(function() {
            // Sign-out successful.
            console.log('signout')
            navigation.navigate('Auth')
        }).catch(function(error) {
            // An error happened.
            console.log('signout error',error)
        });
    };

    const placeholder_levels = {
        label: 'Select a school level...',
        value: null,
        color: '#9EA0A4',
    };
    const placeholder_subjects = {
        label: 'Select a subject...',
        value: null,
        color: '#9EA0A4',
    };

    const _levelOnChange = (value) => {
        setLevel(value)

        /** here connect to the fibo server */

    };
    const _subjectOnChange = (value) => {
        setSubject(value)

        /** here connect to the fibo server */

    };

    const _renderSaveButton = () => {

        return (
            <Button
                title={"Save"}
                onPress={()=>_saveSettings()}
            />
        )
    };

    const _saveSettings = () => {
        firebase.auth().currentUser.updateProfile({
            displayName: name
        })
        let updates = {}
        let user = firebase.auth().currentUser
        updates['/users/'+user.uid+ '/name'] = name
        firebase.database().ref().update(updates);
        Keyboard.dismiss();
        setShowSaved(true)
        setTimeout(() => {
            setShowSaved(false)
        }, 3000)
    }

    const _showSaved = () => {
        if (showSaved){
            return (<Text style={{textAlign:'center'}}>Saved!</Text>)
        } else {
            return null
        }
    }
    const _becomeTutor = () => {
        let userid = firebase.auth().currentUser.uid;
        firebase.database().ref('/tutor_applications/'+userid).set(true, function(){
            setTutorApplied(false)
        });
    };
    const _showTutorInfo = () => {
        if (!tutorApplied){
            return (
                <Card>
                    <Button
                        title={"Become a Tutor"}
                        onPress={()=>_becomeTutor()}
                    />
                </Card>
            )
        } else {
            return (
                <Card>
                    <Text style={{textAlign:'center'}}>Tutor Applied! We will interview tutors and you will receive a mail about this</Text>
                </Card>
            )
        }
    };

    return (
        <SafeAreaView  style={[{flex:1}]}>
            <Card>
                {_showSaved()}
                <View style={{paddingBottom: 20}}>
                    <View>
                        <Input
                            placeholder='Your Name'
                            value={name}
                            onChangeText={(text) => setName(text)}
                        />
                    </View>
                </View>
                {_renderSaveButton()}
                <View style={{paddingTop: 20}}>
                    <Button
                        title="Logout"
                        type="outline"
                        onPress={() => _logout()}
                    />
                    {/*<TouchableOpacity style={{padding: 20, backgroundColor: "#e18aa7"}} onPress={() => _logout() }>*/}
                    {/*    <Text style={{textAlign: "center", color:"#ffffff"}}>LogOut</Text>*/}
                    {/*</TouchableOpacity>*/}
                </View>
            </Card>
            {_showTutorInfo()}
        </SafeAreaView>

    )
}



export class SettingsScreen_old extends React.Component {
  static navigationOptions = {
    title: 'app.json',
  };
  constructor(props) {
    super(props);

    // this.newJWT = this.newJWT.bind(this);
    this.deleteJWT = deviceStorage.deleteJWT.bind(this);

  }

  _logout_from_fibo = () => {
    console.log('logout')
    this.deleteJWT()
    this.props.navigation.navigate('Auth');
  }

  _logout = () => {
      firebase.auth().signOut().then(function() {
          // Sign-out successful.
      }).catch(function(error) {
          // An error happened.
      });
  }

  render() {
    /* Go ahead and delete ExpoConfigView and replace it with your
     * content, we just wanted to give you a quick view of your config */
    return (
        <SafeAreaView style={{margin:10}}>
          <TouchableOpacity style={{padding: 20, backgroundColor: "#e18aa7"}} onPress={() => this._logout()}>
            <Text style={{textAlign: "center", color:"#ffffff"}}>LogOut</Text>
          </TouchableOpacity>
        </SafeAreaView>

    )

  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "#FFF6F0"
    },
    card: {
        margin: 15,
        paddingVertical: 12,
        paddingHorizontal: 10,
        borderWidth: 1,
        borderColor: 'gray',
        borderRadius: 4,
        backgroundColor: '#ffffff',
        shadowColor: "#333",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    }

});

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: 16,
        paddingVertical: 12,
        paddingHorizontal: 10,
        borderWidth: 1,
        borderColor: 'gray',
        borderRadius: 4,
        color: 'black',
        paddingRight: 30, // to ensure the text is never behind the icon
    },
    inputAndroid: {
        fontSize: 16,
        paddingHorizontal: 10,
        paddingVertical: 8,
        borderWidth: 0.5,
        borderColor: 'purple',
        borderRadius: 8,
        color: 'black',
        paddingRight: 30, // to ensure the text is never behind the icon
    },
});