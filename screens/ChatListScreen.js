import React from 'react';

import {
    View,
    Text,
    TouchableOpacity,
    SafeAreaView,
    FlatList,
    Modal,
    TouchableHighlight,
    SectionList,
    ActivityIndicator,
    TextInput, StyleSheet
} from 'react-native';
import { ListItem, Avatar, Button, Image } from 'react-native-elements';
import no_img from '../assets/images/schoolme_logo_nopng.png'

import deviceStorage from "../services/deviceStorage";
import * as firebase from 'firebase';
import RNPickerSelect from "react-native-picker-select";
import {ScrollView} from "react-native-gesture-handler";



export default function ChatListScreen({navigation}) {
    const [unreadLengthClear, setUnreadLengthClear] = React.useState(false);
    const [chatData, setChatData] = React.useState([]);
    const [isFetching, setIsFetching] = React.useState(false);

    React.useEffect(() => {

        updateChatList();

        return function cleanup() {
        };
    }, []);

    React.useEffect(() => {
        const unsubscribe = navigation.addListener('tabPress', e => {
            // Do something
            if (!unreadLengthClear){
                let user = firebase.auth().currentUser;
                let updates = {};
                updates['/chats_unread/'+user.uid] = 0;
                firebase.database().ref().update(updates).then(() => {
                    setUnreadLengthClear(true)
                });
            }

        });

        return unsubscribe;
    }, [navigation]);

    const updateChatList = async () => {
        setIsFetching(true)

        let user = firebase.auth().currentUser
        let chats_as_student = null
        let chats_as_tutor = null
        console.log('bef user_chats query')
        firebase.database().ref('/user_chats/'+user.uid).once('value').then((snapshot) => {
            if (snapshot.exists()){
                let chats = (snapshot.val()) || null;
                let chatDataTemp = []

                if (chats){
                    Object.keys(chats).map((chat_id) => {
                        firebase.database().ref('/members/' + chat_id).once('value').then(function(snapshot){
                            let members = snapshot.val()
                            let chat_info = {
                                _id:chat_id
                            };
                            if (members){
                                Object.keys(members).forEach((member) => {
                                    if (member !== user.uid){
                                        chat_info.other_member_id = member
                                    }
                                });


                                firebase.database().ref('users/' + chat_info.other_member_id).once('value').then(function(snapshot){
                                    let other_user = snapshot.val()
                                    chat_info.name = other_user.name
                                    chat_info.img = other_user.img
                                    chat_info.isTutor = other_user.isTutor
                                    chat_info.tutor_stars = other_user.tutor_stars
                                    chat_info.other_user_school_subject = other_user.school_subject || null;
                                    chat_info.other_user_school_level = other_user.school_level || null;

                                    console.log('other_user::',chat_info)

                                    firebase.database().ref('chats/'+chat_info._id).once('value').then(function(snapshot){
                                        let data = snapshot.val()
                                        chat_info.last_message = data.last_message;

                                        if (chat_info.isTutor){
                                            chat_info.other_user_comment = other_user.about || ''
                                        } else {
                                            chat_info.other_user_comment = data.comment || ''
                                        }

                                        chatDataTemp = [...chatDataTemp, chat_info]
                                        setChatData(chatDataTemp)
                                        setIsFetching(false)
                                    }).catch((err)=>{
                                        console.log('err',err)
                                    });;
                                }).catch((err)=>{
                                    console.log('err',err)
                                });;
                            }
                        }).catch((err)=>{
                            console.log('err',err)
                        });;
                    })
                }
            } else {
                console.log('snapshot NOT exists::');
                setIsFetching(false)
            }
        }).catch((err)=>{
            console.log('err',err)
        }).finally(()=>{
        });

        /** fallback */
        setTimeout(()=>{
            setIsFetching(false)
        }, 2000)
    };

    const organiseChatsByDate = () => {

    };

    const onPressChatList = (chat_data) => {
        if (!unreadLengthClear){
            /** clear unread notification */
            let user = firebase.auth().currentUser;
            let updates = {};
            updates['/chats_unread/'+user.uid] = 0;
            firebase.database().ref().update(updates).then(() => {
                setUnreadLengthClear(true)
            });
        }

        navigation.navigate('Chat', {chat_data:chat_data})


    }

    const _listItem = ({item, index}) => {
        let username = item.name || "Anonymous";
        let shortName = (item.name) ? item.name[0] : '';
        let image = {
            title:shortName
        }
        if (item.img){image.source = {uri:item.img}}
        let subtitle = item.last_message || ''
        return (
            <ListItem
                title={username}
                subtitle={subtitle}
                subtitleStyle={{color: '#717171'}}
                leftAvatar = {image}
                chevron
                onPress={() => onPressChatList(item)}
            />
        )

    }
    const keyExtractor = (item, index) => index.toString()
    const renderSeparator = () => {
        return (
            <View
                style={{
                    height: 1,
                    width: "86%",
                    backgroundColor: "#CED0CE",
                    marginLeft: "14%"
                }}
            />
        );
    }

    const FlatListHeader = () => {
        return (
            <View style={{padding: 10}}>
                <Text style={styles.topTitle}>
                    Chats
                </Text>
            </View>
        );
    }
    return (
        <SafeAreaView style={{flex:1, backgroundColor: '#fff'}}>
            <FlatList
                data={chatData}
                renderItem={_listItem}
                ListHeaderComponent = { FlatListHeader }
                keyExtractor={keyExtractor}
                onRefresh={() => updateChatList()}
                refreshing={isFetching}
                ItemSeparatorComponent={renderSeparator}

            />
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "#FFF6F0"
    },
    topTitle: {
        fontSize: 30,
        fontWeight: '700'
    },
    button_container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    button_green: {
        backgroundColor: '#6EE8B3',
        height: 40,
        flex: 1, justifyContent: 'center',
        alignItems: 'center'
    },
    button_red: {
        backgroundColor: '#E78564',
        height: 40,
        marginRight: 10,
        flex: 1, justifyContent: 'center',
        alignItems: 'center'
    },
    dark_text : {
        color: '#4B4B4B'
    },
    white_text : {
        color: '#FFFFFF'
    },
    strong: {
        fontWeight: "700"
    },
    new_circle: {
        position:'absolute',
        top:-20,
        right:-16,
        height:20,
        width: 20,
        backgroundColor: 'red',
        borderRadius: 10
    },
    card: {
        margin: 15,
        paddingVertical: 12,
        paddingHorizontal: 10,
        borderWidth: 1,
        borderColor: 'gray',
        borderRadius: 4,
        backgroundColor: '#ffffff',
        shadowColor: "#333",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },

    card_comment: {
        paddingBottom: 20
    }

});

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: 16,
        paddingVertical: 12,
        paddingHorizontal: 10,
        borderWidth: 1,
        borderColor: 'gray',
        borderRadius: 4,
        color: 'black',
        paddingRight: 30, // to ensure the text is never behind the icon
    },
    inputAndroid: {
        fontSize: 16,
        paddingHorizontal: 10,
        paddingVertical: 8,
        borderWidth: 0.5,
        borderColor: 'purple',
        borderRadius: 8,
        color: 'black',
        paddingRight: 30, // to ensure the text is never behind the icon
    },
});