import React from 'react';

import {
    Platform,
    View,
    Text,
    TouchableOpacity,
    SafeAreaView,
    FlatList,
    Modal,
    KeyboardAvoidingView,
    TouchableHighlight,
    TouchableNativeFeedback,
    SectionList,
    ActivityIndicator,
    TextInput,
    StyleSheet,
    Keyboard
} from 'react-native';
import {ListItem, Avatar, Button, Image, Rating, Tooltip, Card} from 'react-native-elements';
import no_img from '../assets/images/schoolme_logo_nopng.png'

import deviceStorage from "../services/deviceStorage";
import * as firebase from 'firebase';
import RNPickerSelect from "react-native-picker-select";
import {ScrollView} from "react-native-gesture-handler";
import { Ionicons } from '@expo/vector-icons';
import ChatMsgItem from "../components/ChatMsgItem"
import Subject from "../constants/Subject_name";
import Level from "../constants/School_level";


export default function ChatScreen({route, navigation}) {
    const { chat_data } = route.params;

    const [isLoading, setIsLoading] = React.useState(true);
    const [msgData, setMsgData] = React.useState([]) ;
    const [chatInputText, setChatInputText] = React.useState('');
    const [count, setCount] = React.useState(0);

    React.useLayoutEffect(() => {
        navigation.setOptions({
            headerRight: () => (
                <Button
                    type='clear'
                    onPress={() => {
                        let user = firebase.auth().currentUser
                        firebase.database().ref('/new_sessions/'+chat_data.other_member_id+'/'+user.uid).set(true)
                    }}
                    title="New Session +"
                    containerStyle={{
                        marginBottom:5,
                        marginRight:6,
                    }}
                    titleStyle={{
                        fontSize:16,
                    }}
                />
            ),
        });
    }, [navigation, setCount]);

    React.useEffect(() => {
        navigation.setOptions({ title: ' ', headerBackTitle:' ' })

        let user = firebase.auth().currentUser
        firebase.database().ref('/messages/'+chat_data._id).on('value', (snapshot)=>{
            let chatdata = snapshot.val()
            if(chatdata){
                chatdata = Object.keys(chatdata).map(function(key){
                    let data = chatdata[key]
                    let msgdata = {
                        text:data.text,
                        createdAt:data.createdAt,
                        auth:data.user
                    }
                    if (data.user === user.uid){
                        msgdata.byme = true
                    }
                    return msgdata
                })

                chatdata.reverse();

                setMsgData(chatdata)
            }
            setIsLoading(false)
        });

        return function cleanup() {
            firebase.database().ref('/messages/'+chat_data._id).off()
        };
    }, []);

    const _listItem = ({item, index}) => {
            return (
            <ChatMsgItem
                text={item.text}
                auth={item.auth}
                byme={item.byme}
            />
        )

    }

    const renderSendIcon = () => {
        if (Platform.OS === 'ios'){
            return (<TouchableHighlight
                    underlayColor = 'rgba(255,255,255,0.2)'
                    onPress = {
                        () => {
                            _onPressSend()
                        }
                    }
                >
                    <Ionicons style={styles.sendButton} name="ios-send" />
                </TouchableHighlight>
            );
        } else {
            return (
                <TouchableNativeFeedback
                    underlayColor = 'rgba(255,255,255,0.2)'
                    onPress = {
                        () => {
                            _onPressSend()
                        }
                    }
                >
                    <Ionicons style={styles.sendButton} name="md-send" />
                </TouchableNativeFeedback>
            );
        }
    }
    const _onPressSend = () => {
        let newtext = chatInputText;
        setChatInputText('');
        if (newtext.trim() !== ''){
            let msgs = msgData;
            let newmsg = {
                byme:true,
                auth:firebase.auth().currentUser.displayName,
                text:newtext
            }
            setMsgData([newmsg, ...msgs]);
            Keyboard.dismiss();


            /** todo: send the data to backend */
            let messages = '/messages/'+ chat_data._id + "/";
            var newChatMsgKey = firebase.database().ref().child(messages).push().key;
            let updates = {}
            let timestamp = new Date().getTime()
            let user = firebase.auth().currentUser
            updates[messages + newChatMsgKey] = {
                createdAt: timestamp,
                text:newmsg.text,
                user:user.uid,
                name:user.displayName
            };
            updates['/chats/'+chat_data._id + '/last_message'] = newmsg.text

            firebase.database().ref().update(updates).then(() => {
            });
        }

    }


    const keyExtractor = (item, index) => index.toString()

    const ratingText = () => {
        console.log('chat_data::',chat_data)
        if (!chat_data.isTutor) return <Text style={{color:'#717171', fontSize:16}}>Student</Text>
        return <Text >
            <Text style={{fontSize:18, fontWeight:'400'}}>4.7 </Text>
            <Rating
                imageSize={20}
                readonly
                startingValue={2}
                ratingCount={1}
                type={'custom'}
                // ratingColor={'#4B4B4B'}
            />
        </Text>
    };

    const aboutText = () => {
        if (!chat_data.about) return null
        return <Text style={{paddingBottom:20, paddingLeft:20, paddingRight:20}}>{'"'+chat_data.about+'"'}</Text>

    }

    const userPopOverInfo = () => {
        let username = chat_data.name || "Anonymous";
        let image = {
            title:chat_data.name[0]
        }
        if (chat_data.img){image.source = {uri:chat_data.img}}
        let subject = Subject[chat_data.other_user_school_subject] && Subject[chat_data.other_user_school_subject].en || '';
        let level = Level[chat_data.other_user_school_level] && Level[chat_data.other_user_school_level].en || '';
        return(
            <View style={[styles.card, {width:'100%'}]}>
                <ListItem
                    leftAvatar = {image}
                    title = {username}
                    subtitle={'"'+chat_data.other_user_comment+'"'}
                    subtitleStyle={{fontStyle:'italic'}}
                />
                <Text style={[styles.strong, {fontSize:16, paddingBottom: 5}]}>{subject}</Text>
                <Text style={[{paddingBottom: 15}]}>{level}</Text>
            </View>
        )

    }

    const _listedItem = () => {
        let username = chat_data.name || "Anonymous";
        let image = {
            title:chat_data.name[0]
        }
        if (chat_data.img){image.source = {uri:chat_data.img}}
        return (
            <View style={{backgroundColor:'#fff'}}>
                <Tooltip
                    popover={userPopOverInfo()}
                    withOverlay={true}
                    withPointer={false}
                    overlayColor={'rgba(210,210,210,0.3)'}
                    backgroundColor={'rgba(255,255,255,0)'}
                    containerStyle={{
                        width:'96%',
                        height:250,
                        position:'absolute',
                        left:0,
                        marginTop:10,
                        marginLeft:'2%',
                        marginRight:'2%'
                    }}
                >
                    <ListItem
                        title={username}
                        subtitle={ratingText()}
                        leftAvatar = {image}
                        chevron
                    />
                </Tooltip>
                {aboutText()}
            </View>

        )

    }

    const renderMessageArea = () => {
        if (isLoading){
            return (
                <ActivityIndicator style={styles.loading} />
            )
        } else {
            return (
                <View>
                    {_listedItem()}
                </View>
            )
        }
    }
    return (
        <SafeAreaView style={{flex:1}}>
            {renderMessageArea()}
            <FlatList
                data={msgData}
                renderItem={_listItem}
                keyExtractor={keyExtractor}
                inverted
            />
            <KeyboardAvoidingView behavior="padding" keyboardVerticalOffset={90}>
                <View style={styles.footer}>
                    <TextInput
                        value={chatInputText}
                        onChangeText={text => setChatInputText(text)}
                        onSubmitEditing={() => _onPressSend()}
                        style={styles.input}
                        returnKeyType={ "done" }
                        underlineColorAndroid="transparent"
                        placeholder="Msg"
                    />
                    {renderSendIcon()}
                </View>
            </KeyboardAvoidingView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "#FFF6F0"
    },
    button_container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    button_green: {
        backgroundColor: '#6EE8B3',
        height: 40,
        flex: 1, justifyContent: 'center',
        alignItems: 'center'
    },
    button_red: {
        backgroundColor: '#E78564',
        height: 40,
        marginRight: 10,
        flex: 1, justifyContent: 'center',
        alignItems: 'center'
    },
    dark_text : {
        color: '#4B4B4B'
    },
    white_text : {
        color: '#FFFFFF'
    },
    strong: {
        fontWeight: "700"
    },
    new_circle: {
        position:'absolute',
        top:-20,
        right:-16,
        height:20,
        width: 20,
        backgroundColor: 'red',
        borderRadius: 10
    },
    card: {
        margin: 15,
        paddingVertical: 12,
        paddingHorizontal: 10,
        borderWidth: 1,
        borderColor: 'gray',
        borderRadius: 4,
        backgroundColor: '#ffffff',
        shadowColor: "#333",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },

    card_comment: {
        paddingBottom: 20
    },

    footer: {
        flexDirection: 'row',
        backgroundColor: '#f1f1f1',
    },
    input: {
        paddingHorizontal: 20,
        paddingVertical: 10,
        fontSize: 13,
        flex: 1,
        marginTop:10,
        marginRight:20,
        marginBottom:10,
        marginLeft:20,
        backgroundColor: '#fff',
        borderRadius:20
    },
    sendButton: {
        fontSize: 40,
        paddingLeft:0,
        paddingRight:0,
        paddingTop:10,
        marginRight:20,
        color:'#6bacef'
    },
    loading: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    }

});

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: 16,
        paddingVertical: 12,
        paddingHorizontal: 10,
        borderWidth: 1,
        borderColor: 'gray',
        borderRadius: 4,
        color: 'black',
        paddingRight: 30, // to ensure the text is never behind the icon
    },
    inputAndroid: {
        fontSize: 16,
        paddingHorizontal: 10,
        paddingVertical: 8,
        borderWidth: 0.5,
        borderColor: 'purple',
        borderRadius: 8,
        color: 'black',
        paddingRight: 30, // to ensure the text is never behind the icon
    },
});