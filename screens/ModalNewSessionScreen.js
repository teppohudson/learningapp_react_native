import React from 'react';

import {
    View,
    Text,
    TouchableOpacity,
    SafeAreaView,
    Modal,
    TouchableHighlight,
    SectionList,
    ActivityIndicator,
    TextInput,
    StyleSheet,
    Keyboard
} from 'react-native';
import { Card, Input, Button } from 'react-native-elements';

import * as firebase from 'firebase';
import RNPickerSelect from "react-native-picker-select";
import {ScrollView} from "react-native-gesture-handler";

export default function ModalNewSessionScreen({navigation}) {

    React.useEffect(() => {
        return function cleanup() {
        };
    }, []);

    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <Text style={{ fontSize: 30 }}>This is a new session notification!</Text>
            <Button onPress={() => navigation.goBack()} title="Dismiss" />
        </View>
    );
}

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: 16,
        paddingVertical: 12,
        paddingHorizontal: 10,
        borderWidth: 1,
        borderColor: 'gray',
        borderRadius: 4,
        color: 'black',
        paddingRight: 30, // to ensure the text is never behind the icon
    },
    inputAndroid: {
        fontSize: 16,
        paddingHorizontal: 10,
        paddingVertical: 8,
        borderWidth: 0.5,
        borderColor: 'purple',
        borderRadius: 8,
        color: 'black',
        paddingRight: 30, // to ensure the text is never behind the icon
    },
});