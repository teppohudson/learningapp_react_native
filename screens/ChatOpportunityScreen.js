import React from 'react';

import {
    View,
    Text,
    TouchableOpacity,
    SafeAreaView,
    FlatList,
    Modal,
    TouchableHighlight,
    SectionList,
    ActivityIndicator,
    TextInput, StyleSheet
} from 'react-native';
import { ListItem, Avatar, Card, Button, Icon } from 'react-native-elements';
import deviceStorage from "../services/deviceStorage";
import * as firebase from 'firebase';
import RNPickerSelect from "react-native-picker-select";
import {ScrollView} from "react-native-gesture-handler";
import Subject from '../constants/Subject_name'
import Level from '../constants/School_level'


export default function ChatOpportunityScreen({navigation}) {
    const [newOpportunityData, setNewOpportunityData] = React.useState([]);
    React.useEffect(() => {
        console.log('after render')

        return function cleanup() {
            console.log('cleanup after effect')
        };
    });

    React.useEffect(() => {
        let user = firebase.auth().currentUser
        /** here fetch and listen user preferences */
        firebase.database().ref('/new_tutor_requests/').on('value', function(snapshot) {
            let newdata = []
            let snapshots = (snapshot.val()) || null

            console.log('snapshots::',snapshots)

            if (snapshots !== null){
                Object.keys(snapshots).forEach((key)=>{
                    let itemdata = snapshots[key];
                    itemdata._id = key;
                    if (itemdata._id !== firebase.auth().currentUser.uid && !itemdata.active) { /** don't push user own item */
                        if (new Date().getTime() - itemdata.createdAt < (2*60*60*1000)) { /** check that is not more than 2h since */
                            newdata.push(itemdata)
                        }
                    }
                });
                setNewOpportunityData(newdata);

                // firebase.database().ref('/current_tutor_active_waiting/' + user.uid).once('value', function(active_snapshot){
                //     let active_snapshots = active_snapshot.val()
                //     if (active_snapshots){
                //         Object.keys(active_snapshots).forEach(function(key){
                //             if (snapshots[key]){
                //                 if (new Date().getTime() - snapshots[key].createdAt < (60*60*1000)) { /** check that is not more than 2h since */
                //                 snapshots[key].active = true;
                //                 }
                //             }
                //         })
                //     }
                //     if (snapshots){
                //         Object.keys(snapshots).forEach((key)=>{
                //             let itemdata = snapshots[key];
                //             itemdata._id = key;
                //             if (itemdata._id !== firebase.auth().currentUser.uid) { /** don't push user own item */
                //                 if (new Date().getTime() - itemdata.createdAt < (60*60*1000)) { /** check that is not more than 2h since */
                //                 newdata.push(itemdata)
                //                 }
                //             }
                //         });
                //     }
                //     setNewOpportunityData(newdata);
                // });
            } else {
                setNewOpportunityData(newdata);
            }
        });
        return function cleanup() {
            firebase.database().ref('/new_tutor_requests/').off();
            setNewOpportunityData([]);
        };
    }, []);

    const _removeOpportunity = (_id) => {
        setNewOpportunityData(newOpportunityData.filter(item => item._id !== _id))
        let user = firebase.auth().currentUser;

        firebase.database().ref('/tutor_responses_lists/' + _id + '/' + user.uid).set(null);
        firebase.database().ref('/current_tutor_active_waiting/' + user.uid + '/' + _id).set(null)

        /** todo: have something like *cancelled in this session* locally mapped to state*/

    }

    const _acceptOpportunity = (_id) => {
        console.log('_id:',_id)
        console.log('_id:',newOpportunityData)
        let selectedItem = newOpportunityData.filter(item => item._id === _id);
        console.log('selectedItem',selectedItem);
        let user = firebase.auth().currentUser;

        firebase.database().ref('users/'+ user.uid).once('value', (snapshot)=>{
            let userinfo = snapshot.val()
            if (!userinfo) userinfo = {}
            firebase.database().ref('/tutor_responses_lists/' + _id + '/' + user.uid).set({
                createdAt: new Date().getTime(), ...userinfo
            });

            firebase.database().ref('/current_tutor_active_waiting/' + user.uid + '/' + _id).set(true)
            setNewOpportunityData(newOpportunityData.filter(item => item._id !== _id))
        })
    }

    const _renderCardButtons = (item) => {
        console.log('item:::',item)
        let buttons = []
        buttons.push(
            <TouchableOpacity key={item._id+'_remove'} onPress={() => {_removeOpportunity(item._id)}} style={{flex:1}}>
                <View style={styles.button_red}><Text style={[styles.white_text, styles.strong]}>Remove</Text></View>
            </TouchableOpacity>
        )
        if (!item.active){
            buttons.push(
                <TouchableOpacity key={item._id+'_ok'} onPress={() => {_acceptOpportunity(item._id)}} style={{flex:1}}>
                    <View style={styles.button_green}><Text style={[styles.dark_text,styles.strong]}>OK</Text></View>
                </TouchableOpacity>
            )
        }
        return buttons
    }

    function _opportunityItem ({item, index}) {
        console.log('item:::', item)
        if (new Date().getTime() - item.createdAt > (60*60*1000)) return null  /** check that is not more than 2h since */
        let username = item.name || "Anonymous";
        let shortName = (item.name) ? item.name[0] : '';
        let image = {
            title:shortName
        }
        if (item.img){image.source = {uri:item.img}}
        return (
            <View style={styles.card} key={item._id}>
                <View>
                    {/*<View style={styles.new_circle}></View>*/}
                    <ListItem
                        leftAvatar = {image}
                        title = {username}
                        subtitle={'"'+item.comment+'"'}
                        subtitleStyle={{fontStyle:'italic'}}
                    />
                    {/*<Text style={[styles.card_comment]}>"{item.comment}"</Text>*/}
                    <Text style={[styles.strong, {fontSize:16, paddingBottom: 5}]}>{Subject[item.school_subject].en}</Text>
                    <Text style={[{paddingBottom: 15}]}>{Level[item.school_level].en}</Text>
                </View>
                <View style={styles.button_container}>
                    {_renderCardButtons(item)}
                </View>
            </View>
        )
    }

    const FlatListHeader = () => {
        return (
            <View style={{padding: 10}}>
                <Text style={styles.topTitle}>
                    Jobs
                </Text>
            </View>
        );
    }

    if (newOpportunityData.length === 0){
        return (
            <SafeAreaView  style={{flex:1}}>
                <Card
                    title='Nothing here'
                    image={require('../assets/images/nothing_found.jpg')}>
                    <Text style={{marginBottom: 10}}>
                        We looked everywhere, but could not find anything, sorry.
                    </Text>
                    {/*<Button*/}
                    {/*    buttonStyle={{borderRadius: 0, marginLeft: 0, marginRight: 0, marginBottom: 0}}*/}
                    {/*    title='RELOAD' />*/}
                </Card>
            </SafeAreaView>
        )
    } else {
        return (
            <SafeAreaView  style={{flex:1}}>
                <FlatList
                    data={newOpportunityData}
                    renderItem={_opportunityItem}
                    ListHeaderComponent={FlatListHeader()}
                    keyExtractor={item => item._id}
                />
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: "#FFF6F0"
    },
    topTitle: {
        fontSize: 30,
        fontWeight: '700'
    },
    button_container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    button_green: {
        backgroundColor: '#6EE8B3',
        height: 40,
        flex: 1, justifyContent: 'center',
        alignItems: 'center'
    },
    button_red: {
        backgroundColor: '#E78564',
        height: 40,
        marginRight: 10,
        flex: 1, justifyContent: 'center',
        alignItems: 'center'
    },
    dark_text : {
        color: '#4B4B4B'
    },
    white_text : {
        color: '#FFFFFF'
    },
    strong: {
        fontWeight: "700"
    },
    new_circle: {
        position:'absolute',
        top:-20,
        right:-16,
        height:20,
        width: 20,
        backgroundColor: 'red',
        borderRadius: 10
    },
    card: {
        margin: 15,
        paddingVertical: 12,
        paddingHorizontal: 10,
        borderWidth: 1,
        borderColor: 'gray',
        // borderRadius: 4,
        backgroundColor: '#ffffff',
        shadowColor: "#333",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },

    card_comment: {
        paddingBottom: 20
    }

});

const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: 16,
        paddingVertical: 12,
        paddingHorizontal: 10,
        borderWidth: 1,
        borderColor: 'gray',
        borderRadius: 4,
        color: 'black',
        paddingRight: 30, // to ensure the text is never behind the icon
    },
    inputAndroid: {
        fontSize: 16,
        paddingHorizontal: 10,
        paddingVertical: 8,
        borderWidth: 0.5,
        borderColor: 'purple',
        borderRadius: 8,
        color: 'black',
        paddingRight: 30, // to ensure the text is never behind the icon
    },
});