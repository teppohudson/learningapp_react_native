import { AsyncStorage } from 'react-native';

const deviceStorage = {
    async saveKey(key, valueToSave) {
        console.log('saveKey()')
        try {
            await AsyncStorage.setItem(key, valueToSave);
            return {ok:true, key:key, value:valueToSave}
        } catch (error) {
            console.log('AsyncStorage Error: ' + error.message);
            return {error:error.message, ok:false}
        }
    },

    async loadKey(key) {
        console.log('loadKey()')
        try {
            const value = await AsyncStorage.getItem(key);
            return value
        } catch (error) {
            console.log({error: "loadKeyError"+key});

        }
    },

    async getJWT(){
        console.log('getJWT()')
        try {
            const value = await AsyncStorage.getItem('id_token');
            console.log('getJWT TOKEN::',value);
            return value
        } catch (error) {
            console.log('getJWT error')
        }
    },

    async loadJWT() {
        console.log('loadJWT()')
        try {
            const value = await AsyncStorage.getItem('id_token');
            console.log('LOAD TOKEN::',value)
            return value

            // if (value !== null) {
            //     this.setState({
            //         jwt: value,
            //         loading: false,
            //     });
            // } else {
            //     this.setState({
            //         loading: false,
            //     });
            // }
        } catch (error) {
            console.log('AsyncStorage Error: ' + error.message);
            return null;
            // this.setState({
            //     initialRoute: "Closed"
            // });
        }
    },

    async deleteJWT() {
        try{
            await AsyncStorage.removeItem('id_token')
        } catch (error) {
            console.log('AsyncStorage Error: ' + error.message);
        }
    }
};

export default deviceStorage;
