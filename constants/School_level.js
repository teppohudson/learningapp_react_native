export default {
    yla_7: {
        'en':'7th Grade',
        'fi':'Yläkoulu 7'
    },
    yla_8: {
        'en':'8th Grade',
        'fi':'Yläkoulu 8'
    },
    yla_9: {
        'en':'9th Grade',
        'fi':'Yläkoulu 9'
    },
    lukio_1: {
        'en':'High School 1',
        'fi':'Lukio 1'
    },
    lukio_2: {
        'en':'High School 2',
        'fi':'Lukio 2'
    },
    lukio_3: {
        'en':'High School 3',
        'fi':'Lukio 3'
    },

}