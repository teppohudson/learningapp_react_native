export default {
    math: {
        'en':'Mathematics',
        'fi':'Matematiikka'
    },
    physics: {
        'en':'Physics',
        'fi':'Fysiikka'
    },
    chemistry: {
        'en':'Chemistry',
        'fi':'Kemia'
    },
}