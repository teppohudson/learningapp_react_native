import * as React from 'react';
import { Platform, StatusBar, StyleSheet, View } from 'react-native';
import { SplashScreen } from 'expo';
import * as Font from 'expo-font';
import { Ionicons } from '@expo/vector-icons';
import { NavigationContainer } from '@react-navigation/native';
import {AppNavigator} from './navigation/AppNavigator';

import * as firebase from 'firebase';
// import * as firebase from 'react-native-firebase';

import ApiKeys from './constants/ApiKeys'

import useLinking from './navigation/useLinking';

/**
 * @return {boolean}
 */

export default function App(props) {
  if (!firebase.apps.length) {
    firebase.initializeApp(ApiKeys.FirebaseConfig);
    var database = firebase.database();


    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        console.log('CURRENT USER', user.uid);

      }
      setIsAuthenticated(user);
      setLoadingComplete(true);
      SplashScreen.hide()

    });

  }

  const [isAuthenticated, setIsAuthenticated] = React.useState(false)
  const [isLoadingComplete, setLoadingComplete] = React.useState(false);
  const [initialNavigationState, setInitialNavigationState] = React.useState();
  const containerRef = React.useRef();
  const { getInitialState } = useLinking(containerRef);

  // Load any resources or data that we need prior to rendering the app
  React.useEffect(() => {
    async function loadResourcesAndDataAsync() {
      try {
        SplashScreen.preventAutoHide();

        // Load our initial navigation state
        setInitialNavigationState(await getInitialState());

        // Load fonts
        await Font.loadAsync({
          ...Ionicons.font,
          'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf'),
        });
      } catch (e) {
        // We might want to provide this error information to an error reporting service
        console.warn(e);
      } finally {

      }
    }

    loadResourcesAndDataAsync();
  }, []);

  if (!isLoadingComplete) {
    return null
  } else {
    let route = (isAuthenticated) ? "Home" : "Auth";
    return (
      <View style={styles.container}>
        {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
        <NavigationContainer ref={containerRef} initialState={initialNavigationState}>
          <AppNavigator initialRouteName={route} userRequest={true} tutorOpportunities={true} />
        </NavigationContainer>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
